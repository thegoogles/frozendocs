# Project FrozenFist
## VortexOps

Here at "Frozen Fist" we are designing software for semi-autonomous armored assault vehicles.
The vehicles are designed to operate either remotely with a human pilot or in "robot mode" 
with guidance from an on-board Artificial Intelligence (AI) system.  

[aileen.berwick@smail.rasmussen.edu](aileen.berwick@smail.rasmussen.edu)


> ![FrozenFist Logo](http://lorempixel.com/320/240/city)


> FrozenFist should run well on Mac OS X and Windows OS.  Although,
there are a few issues to be taken care of.

> There is an issue yet to be resolved and that is that the client
cannot run in safe mode.  Due to a bug on Windows OS, the firewall option
should be turned off (on the GUI menu) if frequent crashes are experienced during use.




©FrozenFist 2017# My project's README

### Pushed to BitBucket repository - 1/14/17
